﻿CREATE TABLE [dbo].[HttpResponseData] (
    [HttpResponseDataId] INT           IDENTITY (1, 1) NOT NULL,
    [HttpRequestDataId]  INT           NOT NULL,
    [StatusCode]         INT           NOT NULL,
    [HttpHeaders]        VARCHAR (MAX) NOT NULL,
    [HttpContent]        VARCHAR (MAX) NOT NULL,
    [HttpContentType]    VARCHAR (50)  NOT NULL,
    [HttpContentLength]  BIGINT        NOT NULL,
    [ResponseDate]       DATETIME      NOT NULL,
    [ElapsedTime]        FLOAT (53)    NOT NULL,
    [CreatedAtUtc]       DATETIME      NOT NULL,
    [Error]              VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PKCX_HttpResponseData_HttpResponseDataId] PRIMARY KEY CLUSTERED ([HttpResponseDataId] ASC),
    CONSTRAINT [FK_HttpResponseData_HttpOutboundRequestData] FOREIGN KEY ([HttpRequestDataId]) REFERENCES [dbo].[HttpOutboundRequestData] ([HttpRequestDataId])
);

