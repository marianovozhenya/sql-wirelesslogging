﻿CREATE TABLE [dbo].[HttpInboundRequestData] (
    [HttpRequestDataId]    INT           IDENTITY (1, 1) NOT NULL,
    [RequestUri]           VARCHAR (MAX) NOT NULL,
    [UserId]               INT           NOT NULL,
    [UserHostAddress]      VARCHAR (50)  NOT NULL,
    [XForwardedForAddress] VARCHAR (50)  NOT NULL,
    [ActionName]           VARCHAR (100) NOT NULL,
    [ControllerName]       VARCHAR (100) NOT NULL,
    [HttpMethod]           VARCHAR (10)  NOT NULL,
    [HttpHeaders]          VARCHAR (MAX) NOT NULL,
    [HttpContent]          VARCHAR (MAX) NOT NULL,
    [HttpContentType]      VARCHAR (50)  NOT NULL,
    [HttpContentLength]    BIGINT        NOT NULL,
    [RequestDate]          DATETIME      NOT NULL,
    [Server]               VARCHAR (256) NOT NULL,
    [CreatedAtUtc]         DATETIME      NOT NULL,
    CONSTRAINT [PKCX_HttpInboundRequestData_HttpRequestDataId] PRIMARY KEY CLUSTERED ([HttpRequestDataId] ASC)
);

