﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 6/30/2014
-- Description:	Inserts a row into the table.
-- =============================================
CREATE PROCEDURE [dbo].[InsertHttpResponseData]
	@HttpRequestDataId		INT,
	@StatusCode				INT,
	@HttpHeaders			VARCHAR(MAX),
	@HttpContent			VARCHAR(MAX),
	@HttpContentType		VARCHAR(50),
	@HttpContentLength		BIGINT,
	@ResponseDate			DATETIME,
	@ElapsedTime			FLOAT,
	@Error					VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[HttpResponseData]
			   ([HttpRequestDataId]
			   ,[StatusCode]
			   ,[HttpHeaders]
			   ,[HttpContent]
			   ,[HttpContentType]
			   ,[HttpContentLength]
			   ,[ResponseDate]
			   ,[ElapsedTime]
			   ,[CreatedAtUtc]
			   ,[Error])
		 VALUES
			   (@HttpRequestDataId
			   ,@StatusCode
			   ,@HttpHeaders
			   ,@HttpContent
			   ,@HttpContentType
			   ,@HttpContentLength
			   ,@ResponseDate
			   ,@ElapsedTime
			   ,GetUTCDate()
			   ,@Error)

	SELECT SCOPE_IDENTITY()
END