﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 6/30/2014
-- Description:	Inserts a row into the table.
-- =============================================
CREATE PROCEDURE [dbo].[InsertHttpOutboundRequestData]
	@RequestUri				varchar(max),
	@AuthenticationMethod	varchar(10),
	@UriAuthority			varchar(256),
	@HttpMethod				varchar(10),
	@HttpHeaders			varchar(max),
	@HttpContent			varchar(max),
	@HttpContentType		varchar(50),
	@HttpContentLength		bigint,
	@RequestDate			datetime,
	@Server					varchar(256)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[HttpOutboundRequestData]
			   ([RequestUri]
			   ,[AuthenticationMethod]
			   ,[UriAuthority]
			   ,[HttpMethod]
			   ,[HttpHeaders]
			   ,[HttpContent]
			   ,[HttpContentType]
			   ,[HttpContentLength]
			   ,[RequestDate]
			   ,[Server]
			   ,[CreatedAtUtc])
		 VALUES
			   (@RequestUri
			   ,@AuthenticationMethod
			   ,@UriAuthority
			   ,@HttpMethod
			   ,@HttpHeaders
			   ,@HttpContent
			   ,@HttpContentType
			   ,@HttpContentLength
			   ,@RequestDate
			   ,@Server
			   ,GetUTCDate())

	SELECT SCOPE_IDENTITY()
END